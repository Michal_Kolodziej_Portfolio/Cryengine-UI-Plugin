#pragma once
#include "UIFuryNode.h"

class CShowNode : public IUIFuryNode
{
public:
	CShowNode(string sNodeName, IUIFuryNode *pNewParent, bool show, SInterfaceObject *pRefInterfaceObj, IUIFuryInterface *pNewInterface);

private:
	virtual void Run();

private:
	const bool bShow;
};