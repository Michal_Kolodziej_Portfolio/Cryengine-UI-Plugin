#include "InterfaceObject.h"

SInterfaceObject::SInterfaceObject(int listIdx, string objName, int x, int y, int w, int h, string labelText, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont, int fontSize, string baseImage, string hoverImage, string pressImage, string additionalImage, string dropClass)
	: sName(objName)
	, sLabelBaseColor(labelBaseColor)
	, sLabelHoverColor(labelHoverColor)
	, sLabelPressColor(labelPressColor)
	, sLabelFont(labelFont)
	, sImageBase(baseImage)
	, sImageHover(hoverImage)
	, sImagePress(pressImage)
	, sImageAdditional(additionalImage)
	, iWidth(w)
	, iHeight(h)
	, iFontSize(fontSize)
	, iListIndex(listIdx)
	, sLabelText(labelText)
	, sDropClass(dropClass)
{
}
