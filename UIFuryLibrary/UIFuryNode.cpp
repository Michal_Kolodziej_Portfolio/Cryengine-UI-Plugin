#include "StdAfx.h"
#include "UIFuryNode.h"

IUIFuryNode::IUIFuryNode(string sNodeName, IUIFuryNode *pNewParent, SInterfaceObject *pRefInterfaceObj, IUIFuryInterface *pNewInterface)
	: sName(sNodeName)
	, pInterface(pNewInterface)
{
	pParent = pNewParent;
	pRefObj = pRefInterfaceObj;

	if (pParent)
		pParent->AddChild(this);
}

void IUIFuryNode::AddChild(IUIFuryNode * pChildNode)
{
	if (!pChildNode)
		return;

	children.push_back(pChildNode);
}

void IUIFuryNode::RunNode()
{
	Run();

	for (int i = 0; i < children.size(); i++)
	{
		if (children[i])
		{
			children[i]->RunNode();
		}
	}
}

string IUIFuryNode::GetRefObjName()
{
	if (!pRefObj)
		return "";

	return pRefObj->sName;
}

