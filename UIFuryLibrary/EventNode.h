#pragma once
#include "UIFuryNode.h"

class CEventNode : public IUIFuryNode
{
public:
	CEventNode(string sNodeName, IUIFuryNode *pNewParent, SInterfaceObject *pRefInterfaceObj, IUIFuryInterface *pNewInterface);

private:
	virtual void Run() override;
};